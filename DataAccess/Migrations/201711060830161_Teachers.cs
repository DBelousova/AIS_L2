namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Teachers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Students", "Teacher_Id", c => c.Int());
            CreateIndex("dbo.Students", "Teacher_Id");
            AddForeignKey("dbo.Students", "Teacher_Id", "dbo.Teachers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "Teacher_Id", "dbo.Teachers");
            DropIndex("dbo.Students", new[] { "Teacher_Id" });
            DropColumn("dbo.Students", "Teacher_Id");
            DropTable("dbo.Teachers");
        }
    }
}
