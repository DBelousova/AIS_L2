namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditNameOfDormitory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dormitories", "Name3", c => c.String());
            AddColumn("dbo.Dormitories", "Name4", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dormitories", "Name4");
            DropColumn("dbo.Dormitories", "Name3");
        }
    }
}
