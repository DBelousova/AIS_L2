﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using DataAccess.Migrations;
using Model;

namespace DataAccess
{
    public class StudentRepository : IRepository
    {
        static StudentRepository()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<StudentContext,Configuration>());
        }
        private string PathToDB;
        

        private ICollection<Student> _Students = new ObservableCollection<Student>();
       

        public ICollection<Student> GetAllStudents()
        {

            using (StudentContext db = new StudentContext())
            {
                //Добавление фейковых данных
                Student student = new Student {First_name = "Tom", Group = "КИ15-08Б", Second_name = "Петров"};
                Student student2 = new Student { First_name = "Jerry", Group = "КИ15-08Б", Second_name = "Петров" };
                db.Students.Add(student);
                db.Students.Add(student2);
                //сохранение изменений, здесь можно отслеживать ошибки
                db.SaveChanges();
                //получение данных, обратите внимание на тип users и _Students
                var users = db.Students;
                _Students = users.ToList();
            }
            return _Students;
        }

        public void Add(Student student)
        {
            
        }
    }
}
