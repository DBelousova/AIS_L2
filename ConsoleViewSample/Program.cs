﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using ViewModel;

namespace ConsoleViewSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var BL = new MainWindowViewModel();
            var students = BL.ReturnStudents();
            foreach (var s in students)
            {
                Console.WriteLine(s.First_name);
            }
            Console.WriteLine("Hello World!");
            Console.ReadLine();
        }
    }
}
